# BestCommerce-ESB

BestCommerce, an e-commerce platform where merchants can sell their products online. It is written  in ibm integration toolkit. There are 2 services in project which is mentioned high priority. Mock data has been used within the project. İt is located repository package. There are 3 jar files that should add projet. Files is located in \BestCommerceJava\lib paths.


## Services Content
	
 - Products
 - Discount
 
### Products

**EndPoint**: bestcommerce/v1/products <br />
**Method** : GET  <br />
**Request Params** : <br />

 - merchantId 
 - pageNo 	  
 - pageSize   
 - fieldName  
 - orderBy	  
 
 ### **Discount**
 
 **EndPoint**: /bestcommerce/v1/discount <br />
**Method** : POST  <br />
**Request Body** : <br />
{   <br />
"merchantId" : 1,  <br />
"productId" : 3,	<br />
"startDate": "15.08.2020", <br />
"endDate": "30.09.2020", <br />
"percentage": 25 <br />
}  <br />


