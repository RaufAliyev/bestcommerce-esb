package az.best.commerce.repository;

import java.util.HashMap;
import java.util.Map;

import az.best.commerce.entity.Merchant;

public class MerchantRepository {
	 public static final Map<String,Merchant> merchants = new HashMap<>();
	 
	 static{
		 Merchant merchant = new Merchant(1,"Individual","AAA MMC","Rauf","Baki seh Asif.kuc",
				                          "+994514949394","aliyevrauf739@gmail.com","12345");
		 merchants.put(merchant.getEmail(), merchant);
	 }
}
