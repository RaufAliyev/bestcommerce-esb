package az.best.commerce.repository;

import java.util.ArrayList;

import az.best.commerce.entity.MerchantProduct;


public class MerchantProductRepository {
	public static final ArrayList<MerchantProduct> listProducts = new ArrayList<>();  

	static{
		MerchantProduct mp1 = new MerchantProduct(1, 1, 2200, 0, 15, "Installments");
		MerchantProduct mp2 = new MerchantProduct(1, 2, 800, 0, 10, "Direct");
		MerchantProduct mp3 = new MerchantProduct(1, 3, 20, 0, 100, "Installments");
		MerchantProduct mp4 = new MerchantProduct(1, 4, 40, 0, 80, "Direct");
		MerchantProduct mp5 = new MerchantProduct(1, 5, 50, 0, 60, "Installments");
		MerchantProduct mp6 = new MerchantProduct(1, 6, 100, 0, 8, "Direct");
		MerchantProduct mp7 = new MerchantProduct(1, 7, 1500, 0, 4, "Direct");
		
		listProducts.add(mp1);
		listProducts.add(mp2);
		listProducts.add(mp3);
		listProducts.add(mp4);
		listProducts.add(mp5);
		listProducts.add(mp6);
		listProducts.add(mp7);
	} 
}
