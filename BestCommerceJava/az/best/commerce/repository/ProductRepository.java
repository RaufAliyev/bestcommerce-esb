package az.best.commerce.repository;

import java.util.ArrayList;

import az.best.commerce.entity.Product;


public class ProductRepository {
	public static final ArrayList<Product> listProducts = new ArrayList<>();  

	static{
		Product product1 = new Product(1, "Computer", "", 1);
		Product product2 = new Product(2, "Television", "",  1);
		Product product3 = new Product(3, "T-Shirt", "",2);
		Product product4 = new Product(4, "Trousers", "",  2);
		Product product5 = new Product(5, "Jacket", "",  2);
		Product product6 = new Product(6, "Coat", "",  2);
		Product product7 = new Product(7, "Notebook", "", 1);
		Product product8 = new Product(8, "Bed", "", 3);
		Product product9 = new Product(9, "Table", "", 3);
		Product product10 = new Product(10, "Wardrobe", "", 3);
		
		listProducts.add(product1);
		listProducts.add(product2);
		listProducts.add(product3);
		listProducts.add(product4);
		listProducts.add(product5);
		listProducts.add(product6);
		listProducts.add(product7);
		listProducts.add(product8);
		listProducts.add(product9);
		listProducts.add(product10);
		
	}
}
