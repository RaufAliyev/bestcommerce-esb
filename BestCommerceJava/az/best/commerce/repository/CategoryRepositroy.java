package az.best.commerce.repository;

import java.util.ArrayList;

import az.best.commerce.entity.Category;

public class CategoryRepositroy {
	public static ArrayList<Category> categories = new ArrayList<>();
	 
	 static{
		Category electronic = new Category(1,"Electronics");
		Category fashion = new Category(2,"Fashion");
		Category furniture = new Category(3,"Furniture");
		
		categories.add(electronic);
		categories.add(fashion);
		categories.add(furniture);
	 }
}
