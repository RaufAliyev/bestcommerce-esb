package az.best.commerce.entity;

public class Merchant {
	
	private long id;
	private String type;
	private String name;
	private String ownerName;
	private String address;
	private String phoneNumber;
	private String email;
	private String password;
	
	public Merchant(long id, String type, String name, String ownerName,
			String address, String phoneNumber, String email, String pssword) {
		super();
		this.id = id;
		this.type = type;
		this.name = name;
		this.ownerName = ownerName;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.password = pssword;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPssword() {
		return password;
	}
	public void setPssword(String pssword) {
		this.password = pssword;
	}
	@Override
	public String toString() {
		return "Merchant [id=" + id + ", type=" + type + ", name=" + name
				+ ", ownerName=" + ownerName + ", address=" + address
				+ ", phoneNumber=" + phoneNumber + ", email=" + email
				+ ", pssword=" + password + "]";
	}
}
