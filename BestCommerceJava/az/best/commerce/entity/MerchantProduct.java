package az.best.commerce.entity;

public class MerchantProduct {
	
	private long merchant_id;
	private long product_id;
	private double unitPrice;
	private double discountPrice;
	private long inventory;
	private String paymentOption;
	
	public MerchantProduct(long merchant_id, long product_id, double unitPrice,
			double discountPrice, long inventory, String paymentOption) {
		super();
		this.merchant_id = merchant_id;
		this.product_id = product_id;
		this.unitPrice = unitPrice;
		this.discountPrice = discountPrice;
		this.inventory = inventory;
		this.paymentOption = paymentOption;
	}
	public long getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(long merchant_id) {
		this.merchant_id = merchant_id;
	}
	public long getProduct_id() {
		return product_id;
	}
	public void setProduct_id(long product_id) {
		this.product_id = product_id;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public double getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(double discountPrice) {
		this.discountPrice = discountPrice;
	}
	public long getInventory() {
		return inventory;
	}
	public void setInventory(long inventory) {
		this.inventory = inventory;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
}
