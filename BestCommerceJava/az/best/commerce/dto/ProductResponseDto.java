package az.best.commerce.dto;

import az.best.commerce.entity.Category;

public class ProductResponseDto {
	private long id;
	private String name;
	private String desctiption;
	private double unitPrice;
	private double discountPrice;
	private long inventory;
	private String paymentOption;
	private Category category;
	
	public ProductResponseDto() {
		
	}
	public ProductResponseDto(long id, String name, String desctiption,
			double unitPrice, double discountPrice, long inventory,
			String paymentOption, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.desctiption = desctiption;
		this.unitPrice = unitPrice;
		this.discountPrice = discountPrice;
		this.inventory = inventory;
		this.paymentOption = paymentOption;
		this.category = category;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesctiption() {
		return desctiption;
	}
	public void setDesctiption(String desctiption) {
		this.desctiption = desctiption;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public double getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(double discountPrice) {
		this.discountPrice = discountPrice;
	}
	public long getInventory() {
		return inventory;
	}
	public void setInventory(long inventory) {
		this.inventory = inventory;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "ProductListResponseDto [id=" + id + ", name=" + name
				+ ", desctiption=" + desctiption + ", unitPrice=" + unitPrice
				+ ", discountPrice=" + discountPrice + ", inventory="
				+ inventory + ", paymentOption=" + paymentOption
				+ ", category=" + category + "]";
	}
}
