package az.best.commerce.dto;

import java.util.List;

public class Payload {
	
	private int total;
	private int pageCount;
	private List<ProductResponseDto> products;
	public Payload(int total, int pageCount,
			List<ProductResponseDto> products) {
		super();
		this.total = total;
		this.pageCount = pageCount;
		this.products = products;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public List<ProductResponseDto> getProducts() {
		return products;
	}
	public void setProducts(List<ProductResponseDto> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "Payload [total=" + total + ", pageCount=" + pageCount
				+ ", products=" + products + "]";
	}
}
