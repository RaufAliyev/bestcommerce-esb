package az.best.commerce.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DiscountRequestDto {
	
	@JsonIgnore
	private long id;
	private long merchantId;
	private long productId;
	private String startDate;
	private String endDate;
	private double percentage;
	
	public DiscountRequestDto() {
	}
	public DiscountRequestDto(long id, long merchantId, long productId, String startDate,
			String endDate, double percentage) {
		super();
		this.id = id;
		this.merchantId = merchantId;
		this.productId = productId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.percentage = percentage;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	@Override
	public String toString() {
		return "Discount [id=" + id + ", merchantId=" + merchantId
				+ ", productId=" + productId + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", percentage=" + percentage + "]";
	}
	
	
	
}
