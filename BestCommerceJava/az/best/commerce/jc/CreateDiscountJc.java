package az.best.commerce.jc;


import az.best.commerce.dto.*;
import az.best.commerce.entity.Category;
import az.best.commerce.entity.MerchantProduct;
import az.best.commerce.entity.Product;
import az.best.commerce.repository.CategoryRepositroy;
import az.best.commerce.repository.MerchantProductRepository;
import az.best.commerce.repository.ProductRepository;
import az.best.commerce.util.Common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;

public class CreateDiscountJc extends MbJavaComputeNode {
	
	private ObjectMapper mapper = new ObjectMapper();
	private ServiceResponse serviceResponse = new ServiceResponse();
	private String outJsonString = null;
	
	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");
		
		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;

		MbMessage outMessage = new MbMessage(inMessage);
		outAssembly = new MbMessageAssembly(inAssembly, outMessage);
		try {
			MbElement firstElementByPath = inMessage.getRootElement().getFirstElementByPath("/JSON/Data");
            String outputRootData = new String(firstElementByPath.toBitstream(null, null, null, 1208, 1208, 0),"UTF-8");

            DiscountRequestDto discount = mapper.readValue(outputRootData, DiscountRequestDto.class);
            
            ProductResponseDto productResponseDto = getProductResponse(discount);

            serviceResponse = ServiceResponse.of(true, null, productResponseDto);
            outJsonString  = mapper.writeValueAsString(serviceResponse);
		    Common.jsonOutput(outMessage, outJsonString);
		    out.propagate(outAssembly);
		} catch (Exception e) {
			serviceResponse = ServiceResponse.of(false, e.getMessage(), null);
			try {
				outJsonString  = mapper.writeValueAsString(serviceResponse);
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			Common.jsonErrorOutPut(outMessage,outJsonString,500);
			
			alt.propagate(outAssembly);
		}
	}

	//-----------------------------------getProductResponse----------------------------------------------------------------
	private ProductResponseDto getProductResponse(DiscountRequestDto discount) {
		
		MerchantProduct	merchantProduct = MerchantProductRepository.listProducts.stream().
										  filter(m -> m.getMerchant_id() == discount.getMerchantId() && m.getProduct_id() == discount.getProductId()).
										  findFirst().orElse(null);	
		Product product = ProductRepository.listProducts.stream().filter(p -> p.getId() == merchantProduct.getProduct_id()).findFirst().orElse(null);
		Category category = CategoryRepositroy.categories.stream().filter(c -> c.getId() == product.getCategoryId()).findFirst().orElse(null);
		
		
		ProductResponseDto responseDto = new ProductResponseDto();
		responseDto.setId(product.getId());
		responseDto.setName(product.getName());
		responseDto.setDesctiption(product.getDescription());
		responseDto.setUnitPrice(merchantProduct.getUnitPrice());
		responseDto.setDiscountPrice(getDiscountPrice(merchantProduct.getUnitPrice(),discount.getPercentage()));
		responseDto.setInventory(merchantProduct.getInventory());
		responseDto.setPaymentOption(merchantProduct.getPaymentOption());
		responseDto.setCategory(category);
		return responseDto;
	}
	
	//-----------------------------------getDiscountPrice----------------------------------------------------------------
	private double getDiscountPrice(double unitPrice, double discountPercentage){
		return unitPrice - (unitPrice * discountPercentage)/100;
	}
	

}
