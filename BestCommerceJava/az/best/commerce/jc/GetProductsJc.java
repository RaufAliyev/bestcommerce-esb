package az.best.commerce.jc;

import java.util.List;
import java.util.stream.Collectors;
import az.best.commerce.dto.Payload;
import az.best.commerce.dto.ProductResponseDto;
import az.best.commerce.dto.ServiceResponse;
import az.best.commerce.entity.Category;
import az.best.commerce.entity.MerchantProduct;
import az.best.commerce.entity.Product;
import az.best.commerce.repository.CategoryRepositroy;
import az.best.commerce.repository.MerchantProductRepository;
import az.best.commerce.repository.ProductRepository;
import az.best.commerce.util.Common;
import az.best.commerce.util.DynamicComparator;
import az.best.commerce.util.Partition;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;

public class GetProductsJc extends MbJavaComputeNode {
	
	private ObjectMapper mapper = new ObjectMapper();
	private ServiceResponse serviceResponse = new ServiceResponse();
	private String outJsonString = null;
	
	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");
		
		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		MbMessage inLocalEnvironment = inAssembly.getLocalEnvironment();

		MbMessage outMessage = new MbMessage(inMessage);
		outAssembly = new MbMessageAssembly(inAssembly, outMessage);
		
		try {
			MbElement merchantIdElement = inLocalEnvironment.getRootElement().getFirstElementByPath("/REST/Input/Parameters/merchantId");
			MbElement pagNoElement = inLocalEnvironment.getRootElement().getFirstElementByPath("/REST/Input/Parameters/pageNo");
			MbElement pageSizeElement = inLocalEnvironment.getRootElement().getFirstElementByPath("/REST/Input/Parameters/pageSize");
			MbElement fieldNameElement = inLocalEnvironment.getRootElement().getFirstElementByPath("/REST/Input/Parameters/fieldName");
			MbElement orderByElement = inLocalEnvironment.getRootElement().getFirstElementByPath("/REST/Input/Parameters/orderBy");
			
			if(merchantIdElement == null || pagNoElement==null || pageSizeElement ==null || fieldNameElement == null || orderByElement == null){
				throw new Exception("There is any null paramater");
			}
			
			int pageSize = Integer.valueOf(pageSizeElement.getValueAsString());
			int pageNo = Integer.valueOf(pagNoElement.getValueAsString());
			long id = Long.parseLong(merchantIdElement.getValueAsString());
			String fieldName = fieldNameElement.getValueAsString();
			String orderBy = orderByElement.getValueAsString();
			
			List<ProductResponseDto> products =  MerchantProductRepository.listProducts.stream().
													 filter(m -> m.getMerchant_id() == id && m.getInventory() > 5).
													 map(this::mapProductResponse).collect(Collectors.toList());

			List<ProductResponseDto> sortedPrList = sort(products,fieldName,orderBy);
			
			Partition<ProductResponseDto> parts = Partition.ofSize(sortedPrList, pageSize);
			int pageCount = parts.size();
			int total = products.size();
			List<ProductResponseDto> response  = parts.get(pageNo - 1 );	
			
			serviceResponse = ServiceResponse.of(true, null, new Payload(total, pageCount, response));
			outJsonString  = mapper.writeValueAsString(serviceResponse);
		    Common.jsonOutput(outMessage, outJsonString);
		    
		    out.propagate(outAssembly);
		}catch (Exception e) {
			serviceResponse = ServiceResponse.of(false, e.getMessage(), null);
			try {
				outJsonString  = mapper.writeValueAsString(serviceResponse);
			} catch (JsonProcessingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Common.jsonErrorOutPut(outMessage,outJsonString,500);
			alt.propagate(outAssembly);
		}

	}
	//-----------------------------------sort----------------------------------------------------------------
	private List<ProductResponseDto> sort(List<ProductResponseDto> products, String fieldName, String orderBy) {

        List<ProductResponseDto> productList = products.stream()
                .sorted(new DynamicComparator(fieldName, orderBy))
                .collect(Collectors.toList());

        return productList;
    }

   //-----------------------------------mapProductResponse------------------------------------------------------ 
	private ProductResponseDto mapProductResponse(MerchantProduct temp) {
		Product product = getProductById(temp);
		
		Category category =  getCategoryById(product);
		ProductResponseDto responseDto =  new ProductResponseDto();
		responseDto.setId(temp.getProduct_id());
		responseDto.setName(product.getName());
		responseDto.setDesctiption(product.getDescription());
		responseDto.setUnitPrice(temp.getUnitPrice());
		responseDto.setDiscountPrice(temp.getDiscountPrice());
		responseDto.setInventory(temp.getInventory());
		responseDto.setPaymentOption(temp.getPaymentOption());
		responseDto.setCategory(category);
		return responseDto;
	}

	//-----------------getCategoryById------------------------------
	private Category getCategoryById(Product product) {
		return CategoryRepositroy.categories.stream().
							 filter(c -> c.getId() == product.getCategoryId()).
							 findAny().orElse(null);
	}

	//------------------getCategoryById------------------------------ 
	private Product getProductById(MerchantProduct temp) {
		return ProductRepository.listProducts.stream().
				                  filter(p -> p.getId() == temp.getProduct_id()).
				                  findAny().orElse(null);
	}
}
