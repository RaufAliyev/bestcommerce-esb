package az.best.commerce.util;

import java.lang.reflect.Method;
import java.util.Comparator;
import java.lang.Comparable;

import az.best.commerce.dto.ProductResponseDto;

public class  DynamicComparator implements Comparator<ProductResponseDto> {
    private final String type;
    private final String orderBy;

    public DynamicComparator(String type, String orderBy) {
        this.type = type;
        this.orderBy = orderBy;
    }

    public int compare(ProductResponseDto o1, ProductResponseDto o2) {
        try {
            Method m = o1.getClass().getMethod("get" + type.substring(0, 1).toUpperCase() + type.substring(1));
            Comparable s1 = (Comparable) m.invoke(o1);
            Comparable s2 = (Comparable) m.invoke(o2);
            return orderBy.equalsIgnoreCase("asc") ? s1.compareTo(s2) : s2.compareTo(s1);
        } catch (Exception ex) {
            return 0;
        }
    }

}
