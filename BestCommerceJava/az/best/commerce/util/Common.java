package az.best.commerce.util;

import com.ibm.broker.plugin.MbBLOB;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;

public class Common {
	
	//-----------------------------------jsonOutput----------------------------------------------------------------
	public static void jsonOutput(MbMessage outMessage, String json)
			throws MbException {
		MbElement outRootElement = outMessage.getRootElement();
		outRootElement.delete();
		
		MbElement properties = outMessage.getRootElement().createElementAsFirstChild("Properties");
		properties.createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "ContentType", "application/json; charset=utf-8");
		
		MbElement jsonData = outMessage.getRootElement().createElementAsLastChild(MbBLOB.PARSER_NAME);
		jsonData.createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "BLOB", json.getBytes());
	}
	
	//-----------------------------------jsonErrorOutPut-------------------------------------------------------------
	public static void jsonErrorOutPut(MbMessage outMessage, String json, int statusCode) throws MbException{
		MbElement outRootElement = outMessage.getRootElement();
		outRootElement.delete();
		
		MbElement headerElement = outMessage.getRootElement().createElementAsFirstChild("HTTPReplyHeader");
		headerElement.createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "X-Original-HTTP-Status-Code", statusCode);
		
		MbElement properties = outMessage.getRootElement().createElementAsFirstChild("Properties");
		properties.createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "ContentType", "application/json; charset=utf-8");
		
		MbElement jsonData = outMessage.getRootElement().createElementAsLastChild(MbBLOB.PARSER_NAME);
		jsonData.createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "BLOB", json.getBytes());
		
	}
}
